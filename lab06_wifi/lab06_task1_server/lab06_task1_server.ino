

#include <ESP8266WiFi.h>



// WIFI
char ssid[] = "ut-public";
char pass[] = "";

WiFiClient server(80);
IPAddress IP(192, 168, 4, 15);
IPAddress mask = (255, 255, 255, 0);

String request;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  WiFi.softAP("Wemos_AP_rebriks", "Wemos_comm_rebriks");
  WiFi.softAPConfig(IP, IP, mask);
  server.begin();

  Serial.println("[SUCCESS] Server to wifi connected");
  Serial.println("[SUCCESS] Server started");
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());
  
}

void loop() {
  // put your main code here, to run repeatedly:
  WiFiClient client = server.available();
  
  if (!client) {
    return;
  }
  
  request = client.readStringUntil('\r');
  Serial.println("*****************");
  Serial.println("FROM CLIENT: " + request);
  client.flush();

  String wtf = Serial.readString();
  Serial.println("WTF: " + wtf);
}
