/*  Accesspoint - station communication without router
 *  see: https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/soft-access-point-class.rst
 *       https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/soft-access-point-examples.rst
 *       https://github.com/esp8266/Arduino/issues/504
 *  Works with: station_bare_01.ino
 */ 

#include <Adafruit_NeoPixel.h>


#include <ESP8266WiFi.h>

WiFiServer server(80);
IPAddress IP(192,168,4,15);
IPAddress mask = (255, 255, 255, 0);

//int led_pin = D6;

#define led_pin D6
#define buzz_pin D2

#define rgb_led D7
#define NUMPIXELS 1

Adafruit_NeoPixel pixels(NUMPIXELS, rgb_led, NEO_GRB + NEO_KHZ800);
#define DELAY_VAL 500

String reply;

const int STOP_DIST = 10;
const int SLOWDOWN_DIST = 50;
const int WARNING_DIST = 70;

const int STOP_FREQ = 20;
const int SLOWDOWN_FREQ = 5;
const int WARNING_FREQ = 1;

int r, g, buzz_freq, dist;


int distances[10];

void setup() {
  
  Serial.begin(115200);
  WiFi.mode(WIFI_AP);
  WiFi.softAP("Wemos_AP_rebriks", "rebriks_sheesh");
  WiFi.softAPConfig(IP, IP, mask);
  server.begin();
  
  Serial.println();
  Serial.println("accesspoint_bare_01.ino");
  Serial.println("Server started.");
  Serial.print("IP: ");     Serial.println(WiFi.softAPIP());
  Serial.print("MAC:");     Serial.println(WiFi.softAPmacAddress());
  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, LOW);

  pixels.begin();
  pixels.setBrightness(100);

  pinMode(buzz_pin, OUTPUT);
  digitalWrite(buzz_pin, LOW);
  
}

void loop() {
  //digitalWrite(buzz_pin, LOW);
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  Serial.println("Client connected");
  
  pixels.clear();
  
  String request = client.readStringUntil('\r');
  Serial.println("********************************");
  Serial.println("From the station: " + request);
  //client.flush();

  dist = limit_range(request.toFloat(), STOP_DIST, WARNING_DIST);
  Serial.print("limit dist: ");
  Serial.println(dist);
  r = int(map(dist, STOP_DIST, WARNING_DIST, 255, 0));
  g = int(255 - r);
  Serial.println("red: " + String(r) + " green: " + String(g));
  Serial.println("ARRAY: " + String(distances[0]));

  
  if (dist < WARNING_DIST) {
    //buzz_freq = map(dist, STOP_DIST, WARNING_DIST, STOP_FREQ, WARNING_FREQ);
    buzz_freq = freq_curve(dist);
    tone(buzz_pin, buzz_freq);
    Serial.println("FREQ: " + String(buzz_freq));
  
  } else if (dist >= WARNING_DIST){
    noTone(buzz_pin);
  }
  
  
  
  pixels.setPixelColor(0, pixels.Color(r, g, 0));
  pixels.show();

}

int limit_range(int dist, int low, int high) {
  if (dist > high) {
    return high;
  } else if (dist < low) {
    return low;
  } else {
    return dist;
  }
}

int freq_curve(int dist) {
  float y = exp(-0.1088 * dist + 5);
  return ceil(y);
}
